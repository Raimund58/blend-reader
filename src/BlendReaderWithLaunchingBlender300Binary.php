<?php
/**
 * Copyright (C) 2021 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 *
 **/

namespace SheepItRenderFarm\BlendReader;

use Psr\Log\LoggerInterface;

/**
 * Get data information about a blender.
 * Only support Blender 3.0
 */
class BlendReaderWithLaunchingBlender300Binary extends BlendReader {
    private $SCRIPT30 = __DIR__."/pyscript30.py";
    
    private $logger;
    
    private $path;
    
    private $blenderBinary30x;
    
    private $data;
    
    public function __construct(LoggerInterface $logger, string $b30x) {
        $this->logger = $logger;
        
        $this->blenderBinary30x = $b30x;
        
        $this->data = null;
    }
    
    public function open($path) {
        $this->path = $path;
        return true;
    }
    
    public function getVersion(): String {
        if (is_null($this->data)) {
            $this->data = $this->getInfos();
        }

        // data['version'] is like "blender300", must return "300"

        if (strlen($this->data['version']) > strlen('blender')) {
            return substr($this->data['version'], strlen('blender'));
        }
        return $this->data['version'];
    }
    
    public function getInfos() {
        $output_file = tempnam(sys_get_temp_dir(), "out");
        $script_path = tempnam(sys_get_temp_dir(), "rea");
        
        $path = str_replace(array(' ', '(', ')', "'"), array('\ ', '\(', '\)', '\\\''), $this->path);
        
        copy($this->SCRIPT30, $script_path);
        $command = "OUTPUT_FILE=$output_file BLEND_FILE=$path ".$this->blenderBinary30x." --threads 4 --factory-startup --disable-autoexec -b $path -P $script_path 2>&1";
        
        $output_command = shell_exec($command);
        $contents = file_get_contents($output_file);
        @unlink($output_file);
        @unlink($script_path);
        if ($contents === false) {
            $this->logger->error(__method__.' failed to get data from file '.$this->path.' exists? '.serialize(file_exists($this->path)).' readable? '.serialize(is_readable($path)));
            
            return false;
        }
        
        $ret = json_decode($contents, true);
        
        if (is_array($ret)) {
            if (array_key_exists('can_use_tile', $ret) == false) {
                $ret['can_use_tile'] = false;
            }
            else {
                $ret['can_use_tile'] = ($ret['can_use_tile'] == 'True');
            }
        }
        
        return $ret;
    }
}

