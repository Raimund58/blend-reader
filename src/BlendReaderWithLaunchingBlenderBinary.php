<?php
/**
 * Copyright (C) 2013-2020 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 *
 **/

namespace SheepItRenderFarm\BlendReader;

use Psr\Log\LoggerInterface;

class BlendReaderWithLaunchingBlenderBinary extends BlendReader {
    private $SCRIPT = __DIR__."/pyscript.py";
    private $SCRIPT30 = __DIR__."/pyscript30.py";
    
    private $logger;
    
    private $path;
    private $file;
    private $gzipped;
    
    private $blenderBinary28x;
    private $blenderBinary29x;
    private $blenderBinary30x;
    
    /**
     * Special hack for blender 3.0 zstd compressed file
     * @var bool
     */
    private $special_300 = false;
    
    public function __construct(LoggerInterface $logger, string $b28x, string $b29x, string $b30x) {
        $this->logger = $logger;
        
        $this->blenderBinary28x = $b28x;
        $this->blenderBinary29x = $b29x;
        $this->blenderBinary30x = $b30x;
        
        $this->file = null;
        $this->gzipped = false;
    }
    
    public function open($path) {
        $file = @fopen($path, 'r');
        if (is_resource($file) == false) {
            $this->logger->error('BlendReader::open failed to get data from file '.$path.' exists? '.serialize(file_exists($path)).' readable? '.serialize(is_readable($path)));
            
            return false;
        }
        
        $this->path = $path;
        $this->file = $file;
        $this->special_300 = false;
        
        $magic = $this->read(2);
        if (bin2hex(substr($magic, 0, 2)) == '28b5') { // zstd
            $this->special_300 = true;
        }
        else if (bin2hex(substr($magic, 0, 2)) == '1f8b') { // gzip_magic
            $this->gzipped = true;
            $this->file = gzopen($path, 'r');
        }
        else { // reset the fseek
            $this->file = fopen($path, 'r');
        }
        
        return true;
    }
    
    protected function read($size) {
        if ($this->gzipped) {
            $buf = gzread($this->file, $size);
        }
        else {
            $buf = fread($this->file, $size);
        }
        
        return $buf;
    }
    
    protected function rewind() {
        rewind($this->file);
    }
    
    public function getVersion(): String {
        if ($this->special_300) {
            $blender300 = new BlendReaderWithLaunchingBlender300Binary($this->logger, $this->blenderBinary30x);
            $blender300->open($this->path);
            return $blender300->getVersion();
        }
        
        if ($this->read(7) != 'BLENDER') {
            $this->logger->error(__method__.' not a blend file');
            
            return '';
        }
        
        // BLENDER-v274RENDH.....
        $this->read(2);
        
        $version = $this->read(3);
        
        $this->rewind();
        
        return $version;
    }
    
    public function getInfos() {
        if ($this->special_300 || substr($this->getVersion(), 0, 1) == '3') {
            $blender300 = new BlendReaderWithLaunchingBlender300Binary($this->logger, $this->blenderBinary30x);
            $blender300->open($this->path);
            return $blender300->getInfos();
        }
        
        $buf = $this->read(7);
        if ($buf != 'BLENDER') {
            $this->logger->error('BlendReader::getInfos not a blend file');
            
            return false;
        }
        
        // BLENDER-v274RENDH.....
        $this->read(2);
        $version = $this->read(3);
        
        $output_file = tempnam(sys_get_temp_dir(), "out");
        $script_path = tempnam(sys_get_temp_dir(), "rea");
        
        $path = str_replace(array(' ', '(', ')', "'"), array('\ ', '\(', '\)', '\\\''), $this->path);
        
        // lower than 280 is not supported
        if (substr($version, 0, 2) == "28") {
            copy($this->SCRIPT, $script_path);
            $command = "OUTPUT_FILE=$output_file BLEND_FILE=$path ".$this->blenderBinary28x." --threads 4 --factory-startup -noaudio --disable-autoexec -b $path -P $script_path 2>&1";
        }
        else if (substr($version, 0, 2) == "29") {
            copy($this->SCRIPT, $script_path);
            $command = "OUTPUT_FILE=$output_file BLEND_FILE=$path ".$this->blenderBinary29x." --threads 4 --factory-startup --disable-autoexec -b $path -P $script_path 2>&1";
        }
        else {
            copy($this->SCRIPT30, $script_path);
            $command = "OUTPUT_FILE=$output_file BLEND_FILE=$path ".$this->blenderBinary30x." --threads 4 --factory-startup --disable-autoexec -b $path -P $script_path 2>&1";
        }
        
        $output_command = shell_exec($command);
        $contents = file_get_contents($output_file);
        @unlink($output_file);
        @unlink($script_path);
        if ($contents === false) {
            $this->logger->error('BlendReader::getInfos failed to get data from file '.$this->path.' exists? '.serialize(file_exists($this->path)).' readable? '.serialize(is_readable($path)));
            
            return false;
        }
        
        $ret = json_decode($contents, true);
        
        if (is_array($ret)) {
            $ret['version'] = 'blender'.$version;
            if (array_key_exists('can_use_tile', $ret) == false) {
                $ret['can_use_tile'] = false;
            }
            else {
                $ret['can_use_tile'] = ($ret['can_use_tile'] == 'True');
            }
        }
        
        return $ret;
    }
}

