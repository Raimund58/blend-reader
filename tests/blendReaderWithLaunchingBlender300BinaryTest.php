<?php
/**
 * Copyright (C) 2021 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 *
 **/

use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use PHPUnit\Framework\TestCase;

class blendReaderWithLaunchingBlender300BinaryTest extends TestCase {
    /**
     * @var \SheepItRenderFarm\BlendReader\BlendReaderWithLaunchingBlender300Binary
     */
    private $reader300;
    
    protected function setUp(): void {
        $log = new Logger('blend-reader');
        $log->pushHandler(new StreamHandler('/tmp/blend-reader.log', Logger::DEBUG));

        $this->reader300 = new \SheepItRenderFarm\BlendReader\BlendReaderWithLaunchingBlender300Binary($log,'/opt/blender3.3/rend.exe');
    }
    
    public function testBlender300Compress() {
        $blender_file = __DIR__.'/data/300-compress.blend';
        
        $ret = $this->reader300->open($blender_file);
        $this->assertTrue($ret);
        
        $infos = $this->reader300->getInfos();
        
        $this->assertIsArray($infos);
        $this->assertArrayHasKey('version', $infos);
        $this->assertArrayHasKey('engine', $infos);
        
        $this->assertEquals('blender300', $infos['version']);
        $this->assertEquals('BLENDER_EEVEE', $infos['engine']);
        $this->assertFalse($infos['can_use_tile']);

        $this->assertEquals('300', $this->reader300->getVersion());
    }

    public function test302cycles1920x108040pcstart10end100() {
        $blender_file = __DIR__.'/data/302-cycles-1920x1080-40pc-start10-end100.blend';
        
        $ret = $this->reader300->open($blender_file);
        $this->assertTrue($ret);
        
        $infos = $this->reader300->getInfos();
        
        $this->assertIsArray($infos);
        
        $this->assertArrayHasKey('version', $infos);
        $this->assertArrayHasKey('start_frame', $infos);
        $this->assertArrayHasKey('end_frame', $infos);
        $this->assertArrayHasKey('scene', $infos);
        $this->assertArrayHasKey('output_file_extension', $infos);
        $this->assertArrayHasKey('engine', $infos);
        $this->assertArrayHasKey('resolution_percentage', $infos);
        $this->assertArrayHasKey('resolution_x', $infos);
        $this->assertArrayHasKey('resolution_y', $infos);
        $this->assertArrayHasKey('missing_files', $infos);
        $this->assertArrayHasKey('scripted_driver', $infos);
        $this->assertArrayHasKey('cycles_samples', $infos);
        $this->assertArrayHasKey('have_camera', $infos);
        
        $this->assertEquals('blender302', $infos['version']);
        $this->assertEquals(10, $infos['start_frame']);
        $this->assertEquals(100, $infos['end_frame']);
        $this->assertEquals('Scene', $infos['scene']);
        $this->assertEquals('CYCLES', $infos['engine']);
        $this->assertEquals((int)(1920 * 1080 * 0.40 * 0.40 * 10), $infos['cycles_samples']);
        $this->assertTrue($infos['have_camera']);
    }
    
    public function test302cycles200x100100pcstart1end250compressed() {
        $blender_file = __DIR__.'/data/302-cycles-200x100-100pcstart1-end250-compressed.blend';
        
        $ret = $this->reader300->open($blender_file);
        $this->assertTrue($ret);
        
        $infos = $this->reader300->getInfos();
        
        $this->assertIsArray($infos);
        
        $this->assertArrayHasKey('version', $infos);
        $this->assertArrayHasKey('start_frame', $infos);
        $this->assertArrayHasKey('end_frame', $infos);
        $this->assertArrayHasKey('scene', $infos);
        $this->assertArrayHasKey('output_file_extension', $infos);
        $this->assertArrayHasKey('engine', $infos);
        $this->assertArrayHasKey('resolution_percentage', $infos);
        $this->assertArrayHasKey('resolution_x', $infos);
        $this->assertArrayHasKey('resolution_y', $infos);
        $this->assertArrayHasKey('missing_files', $infos);
        $this->assertArrayHasKey('scripted_driver', $infos);
        $this->assertArrayHasKey('cycles_samples', $infos);
        $this->assertArrayHasKey('have_camera', $infos);
        
        $this->assertEquals('blender302', $infos['version']);
        $this->assertEquals(1, $infos['start_frame']);
        $this->assertEquals(250, $infos['end_frame']);
        $this->assertEquals('CYCLES', $infos['engine']);
        $this->assertEquals(200, $infos['resolution_x']);
        $this->assertEquals(100, $infos['resolution_y']);
        $this->assertEquals((int)(200 * 100 * 1 * 1 * 10), $infos['cycles_samples']);
        $this->assertTrue($infos['have_camera']);
    }
    
    public function test302cycles530x260100pcstart1end100() {
        $blender_file = __DIR__.'/data/302-cycles-530x260-100pc-start1-end100.blend';
        
        $ret = $this->reader300->open($blender_file);
        $this->assertTrue($ret);
        
        $infos = $this->reader300->getInfos();
        
        $this->assertIsArray($infos);
        
        $this->assertArrayHasKey('version', $infos);
        $this->assertArrayHasKey('start_frame', $infos);
        $this->assertArrayHasKey('end_frame', $infos);
        $this->assertArrayHasKey('scene', $infos);
        $this->assertArrayHasKey('output_file_extension', $infos);
        $this->assertArrayHasKey('engine', $infos);
        $this->assertArrayHasKey('resolution_percentage', $infos);
        $this->assertArrayHasKey('resolution_x', $infos);
        $this->assertArrayHasKey('resolution_y', $infos);
        $this->assertArrayHasKey('missing_files', $infos);
        $this->assertArrayHasKey('scripted_driver', $infos);
        $this->assertArrayHasKey('cycles_samples', $infos);
        $this->assertArrayHasKey('have_camera', $infos);
        
        $this->assertEquals('blender302', $infos['version']);
        $this->assertEquals(1, $infos['start_frame']);
        $this->assertEquals(100, $infos['end_frame']);
        $this->assertEquals('Scene', $infos['scene']);
        $this->assertEquals('CYCLES', $infos['engine']);
        $this->assertEquals((int)(530 * 260 * 1 * 1 * 150), $infos['cycles_samples']);
        $this->assertTrue($infos['have_camera']);
    }
    
    public function test302cycles530x26050pcstart1end100SquareSamples() {
        $blender_file = __DIR__.'/data/302-cycles-530x260-50pc-start1-end100-square-samples.blend';
        
        $ret = $this->reader300->open($blender_file);
        $this->assertTrue($ret);
        
        $infos = $this->reader300->getInfos();
        
        $this->assertIsArray($infos);
        
        $this->assertArrayHasKey('version', $infos);
        $this->assertArrayHasKey('start_frame', $infos);
        $this->assertArrayHasKey('end_frame', $infos);
        $this->assertArrayHasKey('scene', $infos);
        $this->assertArrayHasKey('output_file_extension', $infos);
        $this->assertArrayHasKey('engine', $infos);
        $this->assertArrayHasKey('resolution_percentage', $infos);
        $this->assertArrayHasKey('resolution_x', $infos);
        $this->assertArrayHasKey('resolution_y', $infos);
        $this->assertArrayHasKey('missing_files', $infos);
        $this->assertArrayHasKey('scripted_driver', $infos);
        $this->assertArrayHasKey('cycles_samples', $infos);
        $this->assertArrayHasKey('have_camera', $infos);
        
        $this->assertEquals('blender302', $infos['version']);
        $this->assertEquals(1, $infos['start_frame']);
        $this->assertEquals(100, $infos['end_frame']);
        $this->assertEquals('Scene', $infos['scene']);
        $this->assertEquals('CYCLES', $infos['engine']);
        $this->assertEquals((int)(530 * 260 * 0.50 * 0.50 * 1500 * 1500), $infos['cycles_samples']);
        $this->assertTrue($infos['have_camera']);
    }
    
    public function testPathWithQuote() {
        $blender_file = __DIR__."/data/302-path-with-'quote'-cycles-530x260-50pc-start1-end100-square-samples.blend";
        
        $ret = $this->reader300->open($blender_file);
        $this->assertTrue($ret);
        
        $infos = $this->reader300->getInfos();
        
        $this->assertIsArray($infos);
        
        $this->assertArrayHasKey('version', $infos);
        $this->assertArrayHasKey('start_frame', $infos);
        $this->assertArrayHasKey('end_frame', $infos);
        $this->assertArrayHasKey('scene', $infos);
        $this->assertArrayHasKey('output_file_extension', $infos);
        $this->assertArrayHasKey('engine', $infos);
        $this->assertArrayHasKey('resolution_percentage', $infos);
        $this->assertArrayHasKey('resolution_x', $infos);
        $this->assertArrayHasKey('resolution_y', $infos);
        $this->assertArrayHasKey('missing_files', $infos);
        $this->assertArrayHasKey('scripted_driver', $infos);
        $this->assertArrayHasKey('cycles_samples', $infos);
        $this->assertArrayHasKey('have_camera', $infos);
        
        $this->assertEquals('blender302', $infos['version']);
        $this->assertEquals(1, $infos['start_frame']);
        $this->assertEquals(100, $infos['end_frame']);
        $this->assertEquals('Scene', $infos['scene']);
        $this->assertEquals('CYCLES', $infos['engine']);
        $this->assertEquals((int)(530 * 260 * 0.50 * 0.50 * 1500 * 1500), $infos['cycles_samples']);
        $this->assertTrue($infos['have_camera']);
    }
    
    public function testPathWithSpace() {
        $blender_file = __DIR__."/data/302-path-with- -cycles-530x260-50pc-start1-end100-square-samples.blend";
        
        $ret = $this->reader300->open($blender_file);
        $this->assertTrue($ret);
        
        $infos = $this->reader300->getInfos();
        
        $this->assertIsArray($infos);
        
        $this->assertArrayHasKey('version', $infos);
        $this->assertArrayHasKey('start_frame', $infos);
        $this->assertArrayHasKey('end_frame', $infos);
        $this->assertArrayHasKey('scene', $infos);
        $this->assertArrayHasKey('output_file_extension', $infos);
        $this->assertArrayHasKey('engine', $infos);
        $this->assertArrayHasKey('resolution_percentage', $infos);
        $this->assertArrayHasKey('resolution_x', $infos);
        $this->assertArrayHasKey('resolution_y', $infos);
        $this->assertArrayHasKey('missing_files', $infos);
        $this->assertArrayHasKey('scripted_driver', $infos);
        $this->assertArrayHasKey('cycles_samples', $infos);
        $this->assertArrayHasKey('have_camera', $infos);
        
        $this->assertEquals('blender302', $infos['version']);
        $this->assertEquals(1, $infos['start_frame']);
        $this->assertEquals(100, $infos['end_frame']);
        $this->assertEquals('Scene', $infos['scene']);
        $this->assertEquals('CYCLES', $infos['engine']);
        $this->assertEquals((int)(530 * 260 * 0.50 * 0.50 * 1500 * 1500), $infos['cycles_samples']);
        $this->assertTrue($infos['have_camera']);
    }
    
    public function test302cycles24fps() {
        $blender_file = __DIR__.'/data/302-cycles-24fps.blend';
        
        $ret = $this->reader300->open($blender_file);
        $this->assertTrue($ret);
        
        $infos = $this->reader300->getInfos();
        
        $this->assertIsArray($infos);
        
        $this->assertArrayHasKey('version', $infos);
        $this->assertArrayHasKey('framerate', $infos);
        $this->assertArrayHasKey('have_camera', $infos);
        $this->assertArrayHasKey('engine', $infos);
        
        $this->assertEquals('blender302', $infos['version']);
        $this->assertTrue($infos['have_camera']);
        $this->assertEquals('CYCLES', $infos['engine']);
        $this->assertEquals(24, $infos['framerate']);
    }
    
    public function testUnusedEnginesInCompositor() {
        $blender_file = __DIR__.'/data/302-renderEngineTest.blend';
        
        $ret = $this->reader300->open($blender_file);
        $this->assertTrue($ret);
        
        $infos = $this->reader300->getInfos();

        $this->assertIsArray($infos);
        
        $this->assertArrayHasKey('engine', $infos);
        $this->assertArrayNotHasKey('cycles_samples', $infos);

        $this->assertEquals('BLENDER_EEVEE', $infos['engine']);
    }

    public function testMixedEnginesInCompositor() {
        $blender_file = __DIR__.'/data/302-renderEngineTestMixed.blend';
        
        $ret = $this->reader300->open($blender_file);
        $this->assertTrue($ret);
        
        $infos = $this->reader300->getInfos();

        $this->assertIsArray($infos);
        
        $this->assertArrayHasKey('engine', $infos);
        $this->assertArrayNotHasKey('cycles_samples', $infos);

        $this->assertEquals('MIXED', $infos['engine']);
    }

    public function testBlender302CanUseTileCyclesWithCompositing() {
        $blender_file = __DIR__.'/data/302-cycles-compositing.blend';
        
        $ret = $this->reader300->open($blender_file);
        $this->assertTrue($ret);
        
        $infos = $this->reader300->getInfos();
        
        $this->assertIsArray($infos);
        $this->assertArrayHasKey('engine', $infos);
        $this->assertArrayHasKey('can_use_tile', $infos);
        
        $this->assertEquals('CYCLES', $infos['engine']);
        $this->assertFalse($infos['can_use_tile']);
    }
    
    public function testBlender302CanUseTileCyclesWithoutCompositing() {
        $blender_file = __DIR__.'/data/302-cycles-no-compositing.blend';
        
        $ret = $this->reader300->open($blender_file);
        $this->assertTrue($ret);
        
        $infos = $this->reader300->getInfos();
        
        $this->assertIsArray($infos);
        $this->assertArrayHasKey('engine', $infos);
        $this->assertArrayHasKey('can_use_tile', $infos);
        
        $this->assertEquals('CYCLES', $infos['engine']);
        $this->assertTrue($infos['can_use_tile']);
    }
    
    public function testBlender302CanUseTileCycleCompositingDisabled() {
        $blender_file = __DIR__.'/data/302-cycles-compositing-disabled.blend';
        
        $ret = $this->reader300->open($blender_file);
        $this->assertTrue($ret);
        
        $infos = $this->reader300->getInfos();
        
        $this->assertIsArray($infos);
        $this->assertArrayHasKey('engine', $infos);
        $this->assertArrayHasKey('can_use_tile', $infos);
        
        $this->assertEquals('CYCLES', $infos['engine']);
        $this->assertTrue($infos['can_use_tile']);
    }
    
    public function testBlender302CanUseTileCycleCompositingEnableUseNodeDisabled() {
        $blender_file = __DIR__.'/data/302-cycles-compositing-disabled-use-node-disabled.blend';
        
        $ret = $this->reader300->open($blender_file);
        $this->assertTrue($ret);
        
        $infos = $this->reader300->getInfos();
        
        $this->assertIsArray($infos);
        $this->assertArrayHasKey('engine', $infos);
        $this->assertArrayHasKey('can_use_tile', $infos);
        
        $this->assertEquals('CYCLES', $infos['engine']);
        $this->assertTrue($infos['can_use_tile']);
    }
    
    public function testBlender302CanUseTileCyclesWithoutDenoising() {
        $blender_file = __DIR__.'/data/302-cycles-no-denoising.blend';
        
        $ret = $this->reader300->open($blender_file);
        $this->assertTrue($ret);
        
        $infos = $this->reader300->getInfos();
        
        $this->assertIsArray($infos);
        $this->assertArrayHasKey('version', $infos);
        $this->assertArrayHasKey('engine', $infos);
        $this->assertArrayHasKey('can_use_tile', $infos);
        
        $this->assertEquals('blender302', $infos['version']);
        $this->assertEquals('CYCLES', $infos['engine']);
        $this->assertTrue($infos['can_use_tile']);
    }
    
    public function testBlender302CanUseTileCycleDenoising() {
        $blender_file = __DIR__.'/data/302-cycles-denoising.blend';
        
        $ret = $this->reader300->open($blender_file);
        $this->assertTrue($ret);
        
        $infos = $this->reader300->getInfos();
        
        $this->assertIsArray($infos);
        $this->assertArrayHasKey('version', $infos);
        $this->assertArrayHasKey('engine', $infos);
        $this->assertArrayHasKey('can_use_tile', $infos);
        
        $this->assertEquals('blender302', $infos['version']);
        $this->assertEquals('CYCLES', $infos['engine']);
        $this->assertFalse($infos['can_use_tile']);
    }
    
    public function test302Cycles() {
        $blender_file = __DIR__.'/data/302-cycles.blend';
        
        $this->assertTrue($this->reader300->open($blender_file), 'Failed to open blend file '.$blender_file);
        
        $infos = $this->reader300->getInfos();
        
        $this->assertIsArray($infos);
        $this->assertArrayHasKey('version', $infos);
        $this->assertArrayHasKey('engine', $infos);
        
        $this->assertEquals('blender302', $infos['version']);
        $this->assertEquals('CYCLES', $infos['engine']);
    }
    
    public function test302Eevee() {
        $blender_file = __DIR__.'/data/302-eevee.blend';
        
        $this->assertTrue($this->reader300->open($blender_file), 'Failed to open blend file '.$blender_file);
        
        $infos = $this->reader300->getInfos();
        $this->assertIsArray($infos);
        $this->assertArrayHasKey('version', $infos);
        $this->assertArrayHasKey('engine', $infos);
        
        $this->assertEquals('blender302', $infos['version']);
        $this->assertEquals('BLENDER_EEVEE', $infos['engine']);
    }
    
    public function test302Workbench() {
        $blender_file = __DIR__.'/data/302-workbench.blend';
        
        $this->assertTrue($this->reader300->open($blender_file), 'Failed to open blend file '.$blender_file);
        
        $infos = $this->reader300->getInfos();
        
        $this->assertIsArray($infos);
        $this->assertArrayHasKey('version', $infos);
        $this->assertArrayHasKey('engine', $infos);
        
        $this->assertEquals('blender302', $infos['version']);
        $this->assertEquals('BLENDER_WORKBENCH', $infos['engine']);
    }
    
    public function testMissingFile() {
        $blender_file = __DIR__.'/data/302-missing-files.blend';
        
        $this->assertTrue($this->reader300->open($blender_file), 'Failed to open blend file '.$blender_file);
        
        $infos = $this->reader300->getInfos();
        
        $this->assertIsArray($infos);
        $this->assertArrayHasKey('missing_files', $infos);
        
        $this->assertIsArray($infos['missing_files']);
        $this->assertCount(1, $infos['missing_files']);
        $this->assertEquals('//../home/laurent/sheepit-contour.png', $infos['missing_files'][0]);
    }
    
    public function testDetectsActiveFileOutputNodes() {
        $blender_file = __DIR__.'/data/302-creating-images-outside-of-working-dir.blend';
        
        $this->assertTrue($this->reader300->open($blender_file), 'Failed to open blend file '.$blender_file);
        
        $infos = $this->reader300->getInfos();
        
        $this->assertIsArray($infos);
        $this->assertArrayHasKey('has_active_file_output_node', $infos);
        $this->assertTrue($infos['has_active_file_output_node']);
    }
    
    public function testDetectsActiveFileOutputNodesFalse() {
        $blender_file = dirname(__FILE__).'/data/302-cycles-denoising.blend';
        
        $this->assertTrue($this->reader300->open($blender_file), 'Failed to open blend file '.$blender_file);
        
        $infos = $this->reader300->getInfos();
        
        $this->assertIsArray($infos);
        $this->assertArrayHasKey('has_active_file_output_node', $infos);
        $this->assertFalse($infos['has_active_file_output_node']);
    }
    
    public function testDetectsActiveFileOutputNodesInActiveScenes() {
        $blender_file = __DIR__.'/data/302-output_file_node.blend';
        
        $this->assertTrue($this->reader300->open($blender_file), 'Failed to open blend file '.$blender_file);
        
        $infos = $this->reader300->getInfos();
        
        $this->assertIsArray($infos);
        $this->assertArrayHasKey('has_active_file_output_node', $infos);
        $this->assertTrue($infos['has_active_file_output_node']);
    }

    public function testDetectsActiveFileOutputNodesInActiveScenesMuted() {
        $blender_file = __DIR__.'/data/302-output_file_node_muted.blend';
        
        $this->assertTrue($this->reader300->open($blender_file), 'Failed to open blend file '.$blender_file);
        
        $infos = $this->reader300->getInfos();
        
        $this->assertIsArray($infos);
        $this->assertArrayHasKey('has_active_file_output_node', $infos);
        $this->assertFalse($infos['has_active_file_output_node']);
    }

    public function testDetectsGPencilObjects() {
        $blender_file = __DIR__.'/data/302-GPencil_detection.blend';
        
        $this->assertTrue($this->reader300->open($blender_file), 'Failed to open blend file '.$blender_file);
        
        $infos = $this->reader300->getInfos();
        
        $this->assertIsArray($infos);
        $this->assertArrayHasKey('has_GPencil_object', $infos);
        $this->assertTrue($infos['has_GPencil_object']);
    }

    public function testDetectsGPencilObjectsFalse() {
        $blender_file = __DIR__.'/data/302-default_scene.blend';
        
        $this->assertTrue($this->reader300->open($blender_file), 'Failed to open blend file '.$blender_file);
        
        $infos = $this->reader300->getInfos();
        
        $this->assertIsArray($infos);
        $this->assertArrayHasKey('has_GPencil_object', $infos);
        $this->assertFalse($infos['has_GPencil_object']);
    }

    public function testAviJpegOutput() {
        $blender_file = __DIR__.'/data/302-avi.jpg.blend';
        
        $this->assertTrue($this->reader300->open($blender_file), 'Failed to open blend file '.$blender_file);
        
        $infos = $this->reader300->getInfos();
        
        $this->assertIsArray($infos);
        $this->assertArrayHasKey('output_file_format', $infos);
        $this->assertArrayHasKey('version', $infos);
        
        $this->assertEquals('blender302', $infos['version']);
        $this->assertEquals('AVI_JPEG', $infos['output_file_format']);
    }

    public function testAviRawOutput() {
        $blender_file = __DIR__.'/data/302-avi.raw.blend';
    
        $this->assertTrue($this->reader300->open($blender_file), 'Failed to open blend file '.$blender_file);
    
        $infos = $this->reader300->getInfos();
    
        $this->assertIsArray($infos);
        $this->assertArrayHasKey('output_file_format', $infos);
        $this->assertArrayHasKey('version', $infos);
    
        $this->assertEquals('blender302', $infos['version']);
        $this->assertEquals('AVI_RAW', $infos['output_file_format']);
    }
    
    public function testFFmpegOutput() {
        $blender_file = __DIR__.'/data/302-ffmpeg.blend';
        
        $this->assertTrue($this->reader300->open($blender_file), 'Failed to open blend file '.$blender_file);
        
        $infos = $this->reader300->getInfos();
        
        $this->assertIsArray($infos);
        $this->assertArrayHasKey('output_file_format', $infos);
        $this->assertArrayHasKey('version', $infos);
        
        $this->assertEquals('blender302', $infos['version']);
        $this->assertEquals('FFMPEG', $infos['output_file_format']);
    }
    
    public function testPNGOutput() {
        $blender_file = __DIR__.'/data/302-eevee.blend';
        
        $this->assertTrue($this->reader300->open($blender_file), 'Failed to open blend file '.$blender_file);
        
        $infos = $this->reader300->getInfos();
        
        $this->assertIsArray($infos);
        $this->assertArrayHasKey('output_file_format', $infos);
        $this->assertArrayHasKey('version', $infos);
        
        $this->assertEquals('blender302', $infos['version']);
        $this->assertEquals('PNG', $infos['output_file_format']);
    }
    
    public function teststereo3dAnglyph() {
        $blender_file = __DIR__.'/data/302-stereo3d-anglyph.blend';
        
        $ret = $this->reader300->open($blender_file);
        $this->assertTrue($ret);
        
        $infos = $this->reader300->getInfos();
        
        $this->assertIsArray($infos);
        
        $this->assertArrayHasKey('resolution_x', $infos);
        $this->assertArrayHasKey('resolution_y', $infos);
        
        $this->assertEquals(300, $infos['resolution_x']);
        $this->assertEquals(200, $infos['resolution_y']);
    }
    
    public function teststereo3dInterlace() {
        $blender_file = __DIR__.'/data/302-stereo3d-interlace.blend';
        
        $ret = $this->reader300->open($blender_file);
        $this->assertTrue($ret);
        
        $infos = $this->reader300->getInfos();
        
        $this->assertIsArray($infos);
        
        $this->assertArrayHasKey('resolution_x', $infos);
        $this->assertArrayHasKey('resolution_y', $infos);
        
        $this->assertEquals(300, $infos['resolution_x']);
        $this->assertEquals(200, $infos['resolution_y']);
    }
    
    
    public function teststereo3dSideBySide() {
        $blender_file = __DIR__.'/data/302-stereo3d-side-by-side.blend';
        
        $ret = $this->reader300->open($blender_file);
        $this->assertTrue($ret);
        
        $infos = $this->reader300->getInfos();
        
        $this->assertIsArray($infos);
        
        $this->assertArrayHasKey('resolution_x', $infos);
        $this->assertArrayHasKey('resolution_y', $infos);
        
        $this->assertEquals(300 * 2, $infos['resolution_x']);
        $this->assertEquals(200, $infos['resolution_y']);
    }
    
    
    public function teststereo3dTopBottom() {
        $blender_file = __DIR__.'/data/302-stereo3d-top-bottom.blend';
        
        $ret = $this->reader300->open($blender_file);
        $this->assertTrue($ret);
        
        $infos = $this->reader300->getInfos();
        
        $this->assertIsArray($infos);
        
        $this->assertArrayHasKey('resolution_x', $infos);
        $this->assertArrayHasKey('resolution_y', $infos);
        
        $this->assertEquals(300, $infos['resolution_x']);
        $this->assertEquals(200 * 2, $infos['resolution_y']);
    }
    
    public function test302IsOptixEnabled() {
        $blender_file = __DIR__.'/data/302-OptiXDenoiserEnabled.blend';

        $ret = $this->reader300->open($blender_file);
        $this->assertTrue($ret);

        $infos = $this->reader300->getInfos();

        $this->assertIsArray($infos);

        $this->assertArrayHasKey('optix_is_active', $infos);
        $this->assertTrue($infos['optix_is_active']);
    }
    
    public function testGetVersion302() {
		$blender_file = __DIR__.'/data/302-cycles.blend';
	
		$ret = $this->reader300->open($blender_file);
		$this->assertTrue($ret);
	
		$this->assertEquals('302', $this->reader300->getVersion());
	}

    public function testGetVersionTwice() {
        $blender_file = __DIR__.'/data/302-cycles.blend';
        
        $ret = $this->reader300->open($blender_file);
        $this->assertTrue($ret);
        
        $this->assertEquals('302', $this->reader300->getVersion());
        // get version should reset the read to begining of the file
        $this->assertEquals('302', $this->reader300->getVersion());
    }
	
	public function testBlender302Info() {
		$blender_file = __DIR__.'/data/302-cycles.blend';
		
		$ret = $this->reader300->open($blender_file);
		$this->assertTrue($ret);
		
		$infos = $this->reader300->getInfos();
		
		$this->assertIsArray($infos);
		$this->assertArrayHasKey('version', $infos);
		$this->assertArrayHasKey('engine', $infos);
		$this->assertArrayHasKey('can_use_tile', $infos);
		
		$this->assertEquals('blender302', $infos['version']);
		$this->assertEquals('CYCLES', $infos['engine']);
		$this->assertFalse($infos['can_use_tile']); // It is False. The .blend uses Denoising
	}

    public function testBlender330Info() {
        $blender_file = __DIR__.'/data/330-cycles.blend';

        $ret = $this->reader300->open($blender_file);
        $this->assertTrue($ret);

        $infos = $this->reader300->getInfos();

        $this->assertIsArray($infos);
        $this->assertArrayHasKey('version', $infos);
        $this->assertArrayHasKey('engine', $infos);

        $this->assertEquals('blender303', $infos['version']);
        $this->assertEquals('CYCLES', $infos['engine']);
    }
	
    public function active_adaptive_sampling() {
        $blender_file = __DIR__.'/data/302-cycles-active_adaptive_sampling.blend';
        
        $this->assertTrue($this->reader300->open($blender_file), 'Failed to open blend file '.$blender_file);
        
        $infos = $this->reader300->getInfos();
        
        $this->assertTrue($infos['get_adaptive_sampling']);
    }

    public function no_active_adaptive_sampling() {
        $blender_file = __DIR__.'/data/302-cycles-compositing.blend';
        
        $this->assertTrue($this->reader300->open($blender_file), 'Failed to open blend file '.$blender_file);
        
        $infos = $this->reader300->getInfos();
        
        $this->assertFalse($infos['get_adaptive_sampling']);
    }

    public function testGetPasses() {
        $blender_file = __DIR__.'/data/302-passes.blend';

        $ret = $this->reader300->open($blender_file);
        $this->assertTrue($ret);
        $infos = $this->reader300->getInfos();

        $this->assertArrayHasKey('enabled_passes', $infos);
        $this->assertCount(5 + 1, $infos['enabled_passes']); // + 1 for Alpha who is not on the UI but always enabled

        $this->assertArrayHasKey('must_be_disabled_passes', $infos);
        $this->assertCount(0, $infos['must_be_disabled_passes']);
    }

    public function testGetPassesMustDisabled() {
        $blender_file = __DIR__.'/data/302-passes-disabled-passes.blend';

        $ret = $this->reader300->open($blender_file);
        $this->assertTrue($ret);
        $infos = $this->reader300->getInfos();

        $this->assertArrayHasKey('enabled_passes', $infos);

        $this->assertArrayHasKey('must_be_disabled_passes', $infos);
        $this->assertCount(4, $infos['must_be_disabled_passes']);
    }

    public function testGetEXRCodec() {
        $blender_file = __DIR__.'/data/302-passes.blend';

        $ret = $this->reader300->open($blender_file);
        $this->assertTrue($ret);
        $infos = $this->reader300->getInfos();
        $this->assertEquals('OPEN_EXR_MULTILAYER', $infos['output_file_format']);

        $this->assertArrayHasKey('exr_codec', $infos);
        $this->assertEquals("DWAB", $infos['exr_codec']);
    }

    public function testDetectSimulations() {
        $blender_file = __DIR__.'/data/302-detect-sim.blend';

        $ret = $this->reader300->open($blender_file);
        $this->assertTrue($ret);
        $infos = $this->reader300->getInfos();

        $this->assertArrayHasKey('simulations', $infos);
        $this->assertCount(4, $infos['simulations']);
    }

    public function testDetectSimulationsNone() {
        $blender_file = __DIR__.'/data/302-no-sim-detect.blend';

        $ret = $this->reader300->open($blender_file);
        $this->assertTrue($ret);
        $infos = $this->reader300->getInfos();

        $this->assertArrayHasKey('simulations', $infos);
        $this->assertCount(0, $infos['simulations']);
    }
}
